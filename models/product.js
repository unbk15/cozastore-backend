const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    maxlength: 40,
    required: [true, "A product should have a title"],
    trim: true
  },
  price: {
    type: Number,
    validate: {
      validator: Number.parseFloat,
      message: `{VALUE} is not an integer`
    },
    required: [true, "A product should have a price"]
  },
  tag: {
    type: String,
    required: [true, "A product should have a tag"],
    enum: {
      values: ['fashion', 'life-style', 'denim', 'street-style', 'crafts'],
      message: 'Possible Tags: Fashion, Life Style, Denim, Street Style, Crafts'
    }
  },
  color: {
    type: String,
    maxlength: 10,
    required: [true, "A product should have a color"]
  },
  popularity:{
    type: Number,
    min: 1,
    max: 5,
    validate: {
      validator : Number.isInteger,
      message: `{VALUE} is not an integer`
    },
    required: [true, "A product should have a popularity between 1 and 5"],
   
  },
  newness:{
    type: Number,
    min: 0,
    max: 1,
    validate: {
      validator: Number.isInteger,
      message: `{VALUE} is not an integer`
    },
    required: [true, "A product should have a newness"],
  },
  cover: {
    type: String,
    maxlength: 50,
    required: [true, "A product should have a cover image"]
  },
  images: {
    type: Array,
    required: [true, "A product should have between 1 and 3 images"]
  },
  category: {
    type: mongoose.Types.ObjectId,
    ref: "Category",
    required: [true, "A product should have a category"]
  },
  description: {
    type: String,
    maxlength: 150,
    trim: true,
    required: [true, "A product should have a description"]
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("Product", productSchema);
