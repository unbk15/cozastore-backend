const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  category: {
    type: String,
    require: [true, "A product should have a category"],
    unique: true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("Category", categorySchema);
