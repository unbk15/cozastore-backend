const express = require("express");
const app = express();
require("dotenv").config({path: '.env'});
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");

const HttpError = require("./controllers/http-error");

// Middleware

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan("dev"));

// Implement CORS
app.use(cors());
app.options("*", cors());

// Serving static files
app.use(express.static(__dirname + "/uploads/images"));

// Specific Url Routes
const productRoutes = require("./routes/product");
const categoryRoutes = require("./routes/category");

// Routes
app.use("/api/v1/", productRoutes);
app.use("/api/v1/", categoryRoutes);
// Error Handling
app.use((error, req, res, next) => {
  res
    .status(error.code || 500)
    .json({ message: error.message || "An unknown error occured" });
});

mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => {
  // Connect to the server
  const port = process.env.port || 8000;
  app.listen(port, () => { console.log(`Listen to port ${port}`);
  })})
  .catch(err => {
    console.log(err);
  });

