const multer = require("multer");
const uuid = require("uuid/v4");
const sharp = require("sharp");
const HttpError = require("./../controllers/http-error");

// If upload single image

// const MIME_TYPE_MAP = {
//   "image/png": "png",
//   "image/jpg": "jpg",
//   "image/jpeg": "jpeg"
// };

// const multerStorage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, "uploads/images");
//   },
//   filename: (req, file, cb) => {
//     const ext = MIME_TYPE_MAP[file.mimetype];
//     console.log(uuid());
//     cb(null, uuid() + "." + ext);
//   }
// });

// const multerStorage = multer.memoryStorage();

// const multerFilter = (req, file, cb) => {
//   const isValid = !!MIME_TYPE_MAP[file.mimetype];
//   let error = isValid ? null : new Error("Invalid mime type!");
//   cb(error, isValid);
// };

// const upload = multer({
//   storage: multerStorage,
//   fileFilter: multerFilter
// });

// save single uploaded image
// exports.uploadProductCover = upload.single("cover");
// req.file

// If need resizing single image
// exports.resizeProductCover = async(req, res, next) => {
//   if (!req.file) next();
//   const fileName = (req.file.filename = uuid() + ".jpeg");
//   try{await sharp(req.file.buffer)
//     .resize(1200, 1486)
//     .toFormat("jpeg")
//     .jpeg({ quality: 100 })
//     .toFile(`uploads/images/${fileName}`);
// }catch(err => new  HttpError('Image Could not be resized'));
//   next();
// };

// upload multiple images

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpg": "jpg",
  "image/jpeg": "jpeg"
};

const multerStorage = multer.memoryStorage();
const multerFilter = (req, files, cb) => {
  const isValid = !!MIME_TYPE_MAP[files.mimetype];
  let error = isValid ? null : new HttpError("Invalid mime type!");
  cb(error, isValid);
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
});

exports.uploadProductImages = upload.fields([
  { name: "cover", maxCount: 1 },
  { name: "images", maxCount: 3 }
]);
// req.files

exports.resizeProductImages = async (req, res, next) => {
  // 1.Check if there is a cover image file in the request.
  if (!req.files.cover) {
    return next(new HttpError("A product shoud have a cover image"));
  }

  // 2.Check if there are images in the files.request
  if (!req.files.images || req.files.images.length > 3) {
    return next(
      new HttpError("A proudct should have at least a  detailed image")
    );
  }
  // 3.Save the cover to the upload folder.
  try {
    req.body.cover = `${uuid()}-cover.jpeg`;
    await sharp(req.files.cover[0].buffer)
      .resize(1200, 1486)
      .toFormat("jpeg")
      .jpeg({ quality: 100 })
      .toFile(`uploads/images/${req.body.cover}`);
  } catch (err) {
    next(new HttpError(err));
  }

  // 4.Save the images from the files request in the uploads folder
  try {
    req.body.images = [];
    await Promise.all(
      req.files.images.map(async image => {
        const filename = `${uuid()}-image.jpg`;
        await sharp(image.buffer)
          .resize(1200, 1486)
          .toFormat("jpeg")
          .jpeg({ quality: 100 })
          .toFile(`uploads/images/${filename}`);
        req.body.images.push({ id: uuid(), name: filename });
      })
    );
  } catch (err) {
    next(new HttpError(err));
  }
  next();
};
