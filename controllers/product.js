const { validationResult } = require("express-validator");
const Product = require("../models/product");
const HttpError = require("./http-error");
const ObjectId = require("mongoose").Types.ObjectId;

exports.allProducts = async (req, res, next) => {
  try {
    const products = await Product.find({})
                                  .populate({path: 'category'})
                                  .exec();
    res.status(200).json(products);
  } catch (err) {
    next(new HttpError("Products could not be found", 500));
  }
};

exports.addProduct = async (req, res, next) => {
  let { title, price, tag, color, popularity, newness, description, category, cover, images } = req.body;
  
  price = parseFloat(price).toFixed(2);
  popularity = parseInt(popularity);
  newness = parseInt(newness);

  if(price.toString().length > 10) return next(new Error("Price should have max 10 digits", 400));

  try {
    let newProduct = await Product.create({
      title,
      price,
      tag,
      color, 
      popularity,
      newness,
      cover,
      images,
      description,
      category
    });
    res.status(200).json(newProduct);
  } catch (err) {
    next(new HttpError(err.message, 500));
  }
};

exports.productsFilter = async (req, res, next) => {
  let filter = req.query.filter ? req.query.filter : {};
  try {
    const products = await Product.find({ category: new ObjectId(filter) })
      .populate({
        path: "category"
      })
      .exec();
    res.status(200).json(products);
  } catch (err) {
    return next(new HttpError("Something went wrong"));
  }
};

exports.productsSearch = async(req, res, next) =>{
  const searchedWord = req.query.searchWord.trimLeft().trimRight();
  const regex = new RegExp(searchedWord, 'i');
 try{
    const searchResults = await Product.find({ $or:[{title: regex}, {description: regex}]})
                                 .populate({path : "category"})
                                 .exec();       
    res.status(200).json(searchResults);
 }catch(err){
      next(new HttpError("Something went wrong"));
 }
}

exports.advanceSortFilter = async(req, res, next) =>{

 const sort = req.query.sort || {};
 const minPrice = req.query.minPrice || 0;
 let maxPrice;
 let color;
 let tag;
 
 if(req.query.maxPrice){
   maxPrice = req.query.maxPrice;
 }else{
  maxPrice = await Product.find().sort({ "price":-1}).limit(1)
  maxPrice = maxPrice[0].price;
 }

 if(req.query.color){
   color = req.query.color.split(",");
 }else{
   color = ["black", "blue", "grey", "green", "white"];
 }

 if(req.query.tag){
   tag = req.query.tag.split(",");
   console.log(tag);
 }else{
   tag = ["fashion", "life-style", "denim", "street-style", "crafts"]
 } 

  try{
      let query = Product.find({price: {$gte: minPrice, $lte : maxPrice }, color: { $in:  color} , tag: {$in: tag}}).sort(sort).populate({path: 'category'});
      const products = await query;
    res.status(200).json(products);
  }catch(err){
    next(new HttpError(err.message, 500))
  }
}
