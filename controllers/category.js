const Category = require("./../models/category");

exports.addCategory = async (req, res) => {
  const { category } = req.body;
  console.log(category);
  try {
    await Category.create({ category }).then(category => {
      res.status(200).json(category);
    });
  } catch (err) {
    res.status(400).json(err);
  }
};
