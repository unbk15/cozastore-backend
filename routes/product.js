const express = require("express");
const router = express.Router({ strict: true });
const multer = require("multer");
const HttpError = require("./../controllers/http-error");
const {
  uploadProductImages,
  resizeProductImages
} = require("../middleware/file-uploads");

const {
  allProducts,
  addProduct,
  productsFilter, 
  productsSearch, 
  advanceSortFilter
} = require("../controllers/product");

const multerErrors = (err, next) => {
  if (err.field === "cover") {
    return next(new HttpError(`A product should have only a cover image`));
  }
  if (err.field === "images") {
    return next(
      new HttpError(`A product should have max three detailed images`)
    );
  }
};

router.get("/products/", allProducts);
router.post(
  "/add-product",
  (req, res, next) => {
    uploadProductImages(req, res, err => {
      if (err instanceof multer.MulterError) {
        multerErrors(err, next);
      }
      next();
    });
  },
  resizeProductImages,
  addProduct
);
router.get("/filter/", productsFilter);
router.get("/search/", productsSearch);
router.get("/advance-sort-filter/", advanceSortFilter);

module.exports = router;
