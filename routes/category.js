const router = require("express").Router({ strict: true });
const { addCategory } = require("./../controllers/category");

router.post("/add-category", addCategory);

module.exports = router;
